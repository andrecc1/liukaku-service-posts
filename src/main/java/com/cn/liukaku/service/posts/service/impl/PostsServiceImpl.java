package com.cn.liukaku.service.posts.service.impl;


import com.cn.liukaku.common.domain.TbPostsPost;
import com.cn.liukaku.common.mapper.caven.TbPostsPostMapper;
import com.cn.liukaku.common.service.impl.BaseServiceImpl;
import com.cn.liukaku.service.posts.service.PostsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("postsServiceImpl")
@Transactional(readOnly = true)
public class PostsServiceImpl extends BaseServiceImpl<TbPostsPost, TbPostsPostMapper> implements PostsService<TbPostsPost> {
}
