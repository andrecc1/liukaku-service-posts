package com.cn.liukaku.service.posts.mapper.caven;

import com.cn.liukaku.common.domain.TbPostsPost;
import org.springframework.stereotype.Repository;
import tk.mybatis.mymapper.MyMapper;

@Repository("tbPostsPostExtendsMapper")
public interface TbPostsPostExtendsMapper extends MyMapper<TbPostsPost> {

}
