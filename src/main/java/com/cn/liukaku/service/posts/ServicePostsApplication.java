package com.cn.liukaku.service.posts;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import tk.mybatis.spring.annotation.MapperScan;

@EnableSwagger2
@MapperScan(basePackages = {"com.cn.liukaku.common.mapper","com.cn.liukaku.service.posts.mapper"})
//@SpringBootApplication(scanBasePackages = "com.cn.liukaku")
@SpringBootApplication
@EnableEurekaClient
public class ServicePostsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServicePostsApplication.class, args);
    }
}
