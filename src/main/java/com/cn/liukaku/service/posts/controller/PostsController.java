package com.cn.liukaku.service.posts.controller;

import com.cn.liukaku.common.domain.TbPostsPost;
import com.cn.liukaku.common.dto.BaseResult;
import com.cn.liukaku.common.utils.MapperUtils;
import com.cn.liukaku.service.posts.service.PostsService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping(value = "v1/posts")
public class PostsController {

    @Autowired
    private PostsService<TbPostsPost> postsService;

    @RequestMapping(value = "{postGuid}", method = RequestMethod.GET)
    public BaseResult get(@PathVariable(required = true) String postGuid) {

        TbPostsPost tbPostsPost = new TbPostsPost();

        tbPostsPost.setPostGuid(postGuid);

        TbPostsPost obj = postsService.selectOne(tbPostsPost);

        return BaseResult.ok(obj);
    }

    @RequestMapping(method = RequestMethod.POST)
    public BaseResult save(@RequestParam(required = true) String tbPostsPostJson,
                           @RequestParam(required = true) String optsBy) {
        int result = 0;
        TbPostsPost tbPostsPost = null;

        try {
            tbPostsPost = MapperUtils.json2pojo(tbPostsPostJson, TbPostsPost.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (tbPostsPost != null) {

            TbPostsPost tbCheck = new TbPostsPost();

            System.out.println("############"+tbPostsPost.getPostGuid());

            tbCheck.setPostGuid(tbPostsPost.getPostGuid());

            TbPostsPost obj = postsService.selectOne(tbCheck);

            boolean mark = (obj == null);

            System.out.println("############"+mark);

            if (mark) {
                result= postsService.insert(tbPostsPost, optsBy);
            } else {
                result=postsService.update(tbPostsPost, optsBy);
            }

            if(result>0){
                return BaseResult.ok("保存文件成功");
            }
        }

        return BaseResult.ok("保存文件失败");
    }
    @ApiOperation(value="文章服务分页查询接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name="pageNum",value="页码",required = true,dataType = "int",paramType = "path"),
            @ApiImplicitParam(name="pageSize",value="页数",required = true,dataType = "int",paramType = "path"),
            @ApiImplicitParam(name="tbPostsPostJson",value="JSON",required = true,dataType = "String",paramType = "path")
    })
    @RequestMapping(value = "page/{pageNum}/{pageSize}", method = RequestMethod.GET)
    public BaseResult page(@PathVariable(required = true) int pageNum,
                           @PathVariable(required = true) int pageSize,
                           @RequestParam(required = false) String tbPostsPostJson) {

        TbPostsPost tbPostsPost = null;

        if(StringUtils.isNotBlank(tbPostsPostJson)){
            try {
                tbPostsPost=MapperUtils.json2pojo(tbPostsPostJson,TbPostsPost.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        PageInfo pageInfo = postsService.page(pageNum, pageSize, tbPostsPost);

        List<TbPostsPost> list = pageInfo.getList();

        BaseResult.Cursor cursor=new BaseResult.Cursor();

        cursor.setTotal(new Long(pageInfo.getTotal()).intValue());

        cursor.setOffset(pageInfo.getPageNum());

        cursor.setLimit(pageInfo.getPageSize());

        return BaseResult.ok(list,cursor);
    }

}
