package com.cn.liukaku.service.posts.service;

import com.cn.liukaku.common.domain.BaseDomain;
import com.cn.liukaku.common.service.BaseService;


public interface PostsService<T extends BaseDomain> extends BaseService<T> {

}
